import React, { Component } from 'react'
import '../styles/prices.css';

export class Prices extends Component {
    render() {
        return (
            <div>
               <h1 className="prices-head"> Prices of Cinnamon </h1>
                <div className="row">

                    <div class="col-md-4 " >
                        <div class="card style-card prices-image" >
                            <img class="card-img-top" src="http://www.cinnatopia.com/wp-content/uploads/2015/01/01301-370x247.jpg" alt="Card image cap"/>
                            <div class="card-body">
                                <h5 class="card-title">M4</h5>
                                <p class="card-text">1KG = Rs.1900/=</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 " >
                        <div class="card style-card prices-image" >
                            <img class="card-img-top" src="http://www.cinnatopia.com/wp-content/uploads/2015/01/01301-370x247.jpg" alt="Card image cap"/>
                            <div class="card-body">
                                <h5 class="card-title">M5</h5>
                                <p class="card-text">1KG = Rs.1300/=</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 " >
                        <div class="card style-card prices-image" >
                            <img class="card-img-top" src="http://www.cinnatopia.com/wp-content/uploads/2015/01/01301-370x247.jpg" alt="Card image cap"/>
                            <div class="card-body">
                                <h5 class="card-title">ALBA</h5>
                                <p class="card-text">1KG = Rs.3000/=</p>
                            </div>
                        </div>
                    </div>
                    
                </div>

                <div className="row">

                    <div class="col-md-4 " >
                        <div class="card style-card prices-image" >
                            <img class="card-img-top" src="http://www.cinnatopia.com/wp-content/uploads/2015/01/01301-370x247.jpg" alt="Card image cap"/>
                            <div class="card-body">
                                <h5 class="card-title">C4</h5>
                                <p class="card-text">1KG = Rs.3500/=</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 " >
                        <div class="card style-card prices-image" >
                            <img class="card-img-top" src="http://www.cinnatopia.com/wp-content/uploads/2015/01/01301-370x247.jpg" alt="Card image cap"/>
                            <div class="card-body">
                                <h5 class="card-title">C5</h5>
                                <p class="card-text">1KG = Rs.4000/=</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 " >
                        <div class="card style-card prices-image" >
                            <img class="card-img-top" src="http://www.cinnatopia.com/wp-content/uploads/2015/01/01301-370x247.jpg" alt="Card image cap"/>
                            <div class="card-body">
                                <h5 class="card-title">C5 Special</h5>
                                <p class="card-text">5500</p>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        )
    }
}

export default Prices
